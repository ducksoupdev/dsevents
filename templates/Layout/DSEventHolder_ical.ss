BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Duck Soup//NONSGML Christ Church St Andrews//EN
<% loop $CurrentEvents %>BEGIN:VEVENT
UID:event_$ID@parishofhernebay.org.uk
SUMMARY:$Title
DTSTAMP:$ICalDateTime('DateCreated')
<% if $AllDay %>DTSTART;VALUE=DATE:$ICalDate('StartDate')<% else %>DTSTART:$ICalDateTime('StartDate')<% end_if %>
<% if $AllDay %>DTEND;VALUE=DATE:$ICalDate('FinishDate')<% else %>DTEND:$ICalDateTime('FinishDate')<% end_if %>
DESCRIPTION:$Description
LOCATION:<% if $Location %>$Location, $Venue<% else %>$Venue<% end_if %>
SEQUENCE:$Sequence
STATUS:CONFIRMED
END:VEVENT<% end_loop %>
END:VCALENDAR