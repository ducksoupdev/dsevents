<?php
class DSEventPage extends Page {
    
	private $eventTypes = array(); // private array for the event types
	private $eventPageTitle = "";
	private $eventPageDesc = "";
	
    private static $singular_name = "Event page";
    
    private static $plural_name = "Event pages";
    
    private static $allowed_children = array('Page');
	
	private static $db = array(
	    'HideExpiredEvents' => 'Boolean'
	);
	
	private static $defaults = array(
	    'HideExpiredEvents' => true
	);
    
    private static $has_many = array(
    	'Events' => 'DSEvent'
    );
    
    private static $has_one = array(
    	'Parent' => 'DSEventHolder'
    );
    
    private static $icon = "dsevents/images/icons/dseventpage";
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        
        $tEventTypes = array();
        $tEventPageTitle = $this->Title;
        $tEventPageDesc = "";
		
		$parent = $this->Parent();
		if (is_object($parent) && $parent->ClassName == "DSEventHolder") {
			foreach ($parent->EventTypes() as $t) {
				$tEventTypes[] = $t->EventType;
			}
		}

		// add the EventPage HTML content minus the H1
		$h1pattern = '/<h1>[^>]+<\/h1>/i';
		$tEventPageDesc = preg_replace($h1pattern, "", $this->Content);
		
		// add Image
		$uploadField = new UploadField('Image', 'Image');
		$uploadField->getValidator()->setAllowedExtensions(array('jpg', "JPG", 'jpeg', "JPEG", 'png', "PNG", 'gif', "GIF"));
		$fields->addFieldToTab('Root.Image', $uploadField);
        
		$gfDetailForm = new GridFieldDetailForm();
        $gridFieldConfig = GridFieldConfig::create()->addComponents(
      		new GridFieldToolbarHeader(),
      		new GridFieldAddNewButton('toolbar-header-right'),
      		new GridFieldSortableHeader(),
      		new GridFieldDataColumns(),
      		new GridFieldPaginator(10),
      		new GridFieldEditButton(),
      		new GridFieldDeleteAction(),
      		$gfDetailForm
    	);
		
		// add a call back to the edit form for the event types field
		$gfDetailForm->setItemEditFormCallback(function($form, $component) use ($tEventTypes, $tEventPageTitle, $tEventPageDesc) {
			$form->Fields()->fieldByName("EventType")->setSource($tEventTypes);
		});
    
    	$gridField = new GridField("Events", "Event list:", $this->Events(), $gridFieldConfig);
    	
    	$hideEventPage = new FieldGroup(new CheckboxField("HideExpiredEvents", "Don't show page if events have expired"));
    	$hideEventPage->setTitle("Expired events");
    	
    	$fields->addFieldToTab("Root.Events", $hideEventPage);
    	$fields->addFieldToTab("Root.Events", $gridField);
        
        return $fields;
    }
    
}
 
class DSEventPage_Controller extends Page_Controller {

	private static $allowed_actions = array (
    );
 
    public function init() {
    	parent::init();
    }
    
	public function CurrentEvents() {
		// $children will be a DataObjectSet
		$children = $this->Events("(StartDate >= DATE(NOW()))");
	
		if( !$children )
			return null; // no children, nothing to work with
	
		// sort the DataObjectSet
		// see http://doc.silverstripe.org/sapphire/en/reference/dataobjectset
		$children->sort('StartDate', 'ASC');
	
		// return sorted set
		return $children;
	}
	
	public function ical(SS_HTTPRequest $request) {
		$this->response->addHeader("Content-Type", "text/calendar");
		//$this->response->addHeader("Content-Type", "text/plain");
		
		$events = null;
		$id = $request->param("ID");
		if ($id) {
		    $events = DSEvent::get()->filter(array("ID" => $id));
		    if ($events) {
		        $this->response->addHeader("Content-Disposition", "inline; filename=\"Event_{$id}.ics\"");
		        return $this->customise(array(
		                'EventPage' => $this,
		                'LocalEvents' => $events
		        ))->renderWith('DSEventPage_ical');
		    } else {
		        return $this->httpError(404, "Event not found with ID: {$id}");
		    }
		} else {
    		$this->response->addHeader("Content-Disposition", "inline; filename=\"" . $this->Title . ".ics\"");
    		
    		$events = $this->Events();
    		if ($events) {
    			$events->sort('StartDate', 'ASC');
    		}
    		
    		return $this->customise(array(
    		        'EventPage' => $this,
    		        'LocalEvents' => $events
    		))->renderWith('DSEventPage_ical');
		}
	}
}
