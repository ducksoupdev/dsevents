<?php
class DSEventType extends DataObject {
	
	private static $singular_name = "Event type";
    
    private static $plural_name = "Event types";
	
	private static $db = array(
		'EventType' => 'Text'
    );
    
    private static $has_one = array(
    	'Parent' => 'DSEventHolder'
    );
    
    private static $summary_fields = array(
    	'EventType' => 'Event Type'
    );
    
    public function getCMSFields() {
        return new FieldList(new TextField('EventType', 'Event Type'));
    }
	
}
