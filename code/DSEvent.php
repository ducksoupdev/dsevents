<?php
class DSEvent extends DataObject {
    
	private static $singular_name = "Event";
    
    private static $plural_name = "Events";

    private static $default_sort = "StartDate DESC";

    private static $default_order = "desc";
	
	private static $db = array(
		'Title' => 'Text',
    	'StartDate' => 'Date',
    	'StartTime' => 'Time',
    	'FinishDate' => 'Date',
    	'FinishTime' => 'Time',
		'DateTimeToBeConfirmed' => 'Boolean',
    	'AllDay' => 'Boolean',
    	'NumDays' => 'Int',
    	'Location' => 'Text',
    	'Venue' => 'Text',
    	'EventType' => 'Text',
    	'Description' => 'HTMLText',
		'DateCreated' => 'SS_Datetime',
		'LastUpdated' => 'SS_Datetime',
		'Sequence' => 'Int',
        'PublishToFacebook' => 'Boolean',
        'FirstPublishedToFacebook' => 'SS_Datetime',
        'LastPublishedToFacebook' => 'SS_Datetime',
        'FacebookEventId' => 'Varchar(255)'
    );
    
    private static $has_one = array(
    	'Parent' => 'DSEventPage',
    	'LinkedPage' => 'SiteTree'
    );
    
    private static $summary_fields = array(
    	'Title' => 'Title',
    	'StartDate' => 'Start Date',
    	'StartTime' => 'Start Time',
    	'Location' => 'Location'
    );
	
	public function validate() {
        $result = parent::validate();
        if (!$this->Title || $this->Title == "") {
            $result->error("Title is a required field");
            return $result;
        }
        return $result;
    }
    
    public function getCMSFields() {
        $sd = new DateField('StartDate', 'Start Date (dd/MM/yyyy)');
    	$fd = new DateField('FinishDate', 'Finish Date (dd/MM/yyyy - optional)');
    	$desc = new HTMLEditorField('Description', 'Description (optional)');
    	$allDayGroup = new FieldGroup(new CheckboxField("AllDay", "All Day Event"));

        $eventTypes = DataObject::get('DSEventType');
        $evtm = array();
        if (!empty($eventTypes)) {
            $evtm = $eventTypes->map('EventType', 'EventType');
        }

    	$etdf = new DropdownField('EventType', 'Event Type', $evtm);
    	$etdf->setEmptyString('(Choose)');
        
        $fields = new FieldList(
        	new TextField('Title', 'Title'),
        	$sd,
        	new TimeField('StartTime', 'Start Time (HH:mm)'),
        	$fd,
        	new TimeField('FinishTime', 'Finish Time (HH:mm - optional)'),
        	$allDayGroup,
        	new NumericField('NumDays', 'Number of Days (optional)'),
        	new TextField('Location', 'Location (optional)'),
        	new TextField('Venue', 'Venue'),
        	$etdf,
        	new TreeDropdownField('LinkedPageID', 'Page for more info (optional)', 'SiteTree'),
            new CheckboxField('PublishToFacebook', 'Publish to Facebook'),
        	$desc
        );
        
        $allDayGroup->setTitle("All Day");
        $desc->addExtraClass('stacked');
        
        $sd->setConfig('showcalendar', true);
        $sd->setConfig('showdropdown', true);
        $sd->setConfig('dateformat', 'dd/MM/YYYY');
        
        $fd->setConfig('showcalendar', true);
        $fd->setConfig('showdropdown', true);
        $fd->setConfig('dateformat', 'dd/MM/YYYY');
        
        return $fields;
    }
	
	public function onBeforeWrite() {
		$this->LastUpdated = date('Y-m-d H:i:s');
        if (!$this->DateCreated) $this->DateCreated = date('Y-m-d H:i:s');
		
		// update the sequence field for iCal
		if (!$this->Sequence) {
			$this->Sequence = 0;
		} else {
			$this->Sequence = $this->Sequence + 1;
		}
		
		parent::onBeforeWrite();
	}
    
    public function FormattedDate() {
    	if ($this->NumDays > 1) {
    		return date('l jS F Y', strtotime($this->StartDate)) . " to " . date('l jS F Y', strtotime($this->FinishDate)) . " (" . $this->NumDays . " days)";
    	} else {
    		return date('l jS F Y', strtotime($this->StartDate));
    	}
    }
    
    public function FormattedTime() {
    	if ($this->FinishDate && $this->FinishTime) {
    		return date('g:i a', strtotime($this->StartDate . " " . $this->StartTime)) . " to " . date('g:i a', strtotime($this->FinishDate . " " . $this->FinishTime));
    	} else {
    		return date('g:i a', strtotime($this->StartDate . " " . $this->StartTime));
    	}
    }
	
	public function ICalDateTime($fieldName) {
		if ($fieldName == "DateCreated") {
			return date("Ymd\THis", strtotime($this->DateCreated));
		} elseif ($fieldName == "StartDate") {
			return date("Ymd\THis", strtotime($this->StartDate . " " . $this->StartTime));
		} elseif ($fieldName == "FinishDate") {
			return date("Ymd\THis", strtotime($this->FinishDate . " " . $this->FinishTime));
		}
	}
	
	public function ICalDate($fieldName) {
		if ($fieldName == "StartDate") {
			return date("Ymd", strtotime($this->StartDate . " " . $this->StartTime));
		} elseif ($fieldName == "FinishDate") {
			return date("Ymd", strtotime($this->FinishDate . " " . $this->FinishTime));
		}
	}
    
    public function FormattedLocation() {
    	if ($this->Location && $this->Venue) {
    		return $this->Venue . ", " . $this->Location;
    	} else if ($this->Location && !$this->Venue) {
    		return $this->Location;
    	} else if (!$this->Location && $this->Venue) {
    		return $this->Venue;
    	}
    	return null;
    }
	
}
