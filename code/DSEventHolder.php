<?php
/**
 * Defines the DSEventHolder page type
 */
class DSEventHolder extends Page {

    private static $db = array(
		"IncludeiCalSubscribeLink" => "Boolean",
        "LayoutType" => "Varchar(64)",
        "LayoutStyle" => "Varchar(64)"
    );

    private static $has_one = array(
    );
    
    private static $has_many = array(
    	'EventTypes' => 'DSEventType'
    );
    
    private static $singular_name = "Event page holder";
    
    private static $plural_name = "Event page holders";
    
    private static $allowed_children = array('Page');
    
    private static $icon = "dsevents/images/icons/dseventpageholder";
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
		
		// Show resource name as title - boolean
    	$fields->addFieldToTab("Root.Main", $icalSub = new FieldGroup(new CheckboxField("IncludeiCalSubscribeLink", "Show iCalendar subscribe link")), "Content");
    	$icalSub->setTitle("Subscribe");

        $fields->addFieldToTab("Root.Layout", new DropdownField("LayoutType", "Type", array("child-links" => "Allow child event pages to show events", "show-all" => "Show all events on this page")));
        $fields->addFieldToTab("Root.Layout", new DropdownField("LayoutStyle", "Style", array("by-month" => "Group events by month", "show-all" => "List all events")));

        return $fields;
    }
    
    public function IsSecondItem() {  
    	if ($this->iteratorPos == 1) {
    		return true;
    	}
    	
    	if ($this->iteratorPos < 2) {
    		return false;
    	}
    	
    	if ($this->iteratorPos % 2) {
    		return true;
  		} else {
  			return false;
  		}
	}

}
  
class DSEventHolder_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
    );
 
    public function init() {
    	parent::init();
    }
    
    public function Children() {
        $children = $this->ChildrenOf($this->ID);
        $eventPages = new ArrayList();
        $cit = $children->getIterator();
        foreach ($cit as $c) {
            if ($c->class == "DSEventPage") {
                // do we display the page if all events have expired
                if ($c->HideExpiredEvents) {
                    $currentEvents = $c->Events("(StartDate >= DATE(NOW()))");
                    if ($currentEvents && $currentEvents->count() > 0) {
                        $eventPages->add($c);
                    }
                } else {
                    $eventPages->add($c);
                }
            }
        }
        SS_Log::log(print_r($eventPages, true), SS_Log::NOTICE);
        return $eventPages;
    }
	
	public function ical(SS_HTTPRequest $request) {
		$this->response->addHeader("Content-Type", "text/calendar");
		//$this->response->addHeader("Content-Type", "text/plain");

        $id = $request->param("ID");
        if ($id) {
            $events = DSEvent::get()->filter(array("ID" => $id));
            if ($events) {
                $this->response->addHeader("Content-Disposition", "inline; filename=\"Event_{$id}.ics\"");
                return $this->customise(array(
                    'CurrentEvents' => $events
                ))->renderWith('DSEventHolder_ical');
            } else {
                return $this->httpError(404, "Event not found with ID: {$id}");
            }
        } else {
            $this->response->addHeader("Content-Disposition", "inline; filename=\"Events.ics\"");

            $events = DSEvent::get("DSEvent", "(StartDate >= DATE(NOW()))")->sort('StartDate', 'ASC');
            return $this->customise(array(
                "CurrentEvents" => $events
            ))->renderWith('DSEventHolder_ical');
        }

	}

    public function CurrentEvents() {
        // $children will be a DataObjectSet
        $children = DSEvent::get("DSEvent", "(StartDate >= DATE(NOW()))");

        if( !$children )
            return null; // no children, nothing to work with

        // sort the DataObjectSet
        // see http://doc.silverstripe.org/sapphire/en/reference/dataobjectset
        $children->sort('StartDate', 'ASC');

        // group by month if we need to
        if ($this->LayoutStyle == "by-month") {
            require_once 'Zend/Date.php';

            $monthEvents = array();
            foreach ($children as $e) {
                $key = "";
                if ($e->StartDate != "") {
                    $key = date("Ym", strtotime($e->StartDate));
                }
                if (isset($monthEvents[$key])) {
                    $monthEvents[$key][] = array();
                    array_push($monthEvents[$key]["Events"], $e);
                } else {
                    $zd = new Zend_Date(strtotime($e->StartDate));
                    $title = $zd->toString("MMMM y");
                    SS_Log::log("Full date: {$title}", SS_Log::NOTICE);
                    $monthEvents[$key] = array("Title" => $title, "Events" => array($e));
                }
            }

            // build arraydata objects
            $nme = array();
            foreach ($monthEvents as $key => $val) {
                array_push($nme, array("Title" => $val["Title"], "Events" => new ArrayList($val["Events"])));
            }
            return new ArrayList($nme);
        }

        // return sorted set
        return $children;
    }
	
}
